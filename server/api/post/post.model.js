'use strict';

import mongoose from 'mongoose';
var textSearch = require('mongoose-text-search');

var PostSchema = new mongoose.Schema({
  title: String,
  category: String,
  body : String,
  mainImage : String,
  author : String,
  date : String,
  comments:[]
});

PostSchema.plugin(textSearch);
PostSchema.index({ body: 'text' });

export default mongoose.model('Post', PostSchema);
