/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/post              ->  index
 * POST    /api/post              ->  create
 * GET     /api/post/:id          ->  show
 * PUT     /api/post/:id          ->  update
 * DELETE  /api/post/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Post from './post.model';
var moment = require('moment');


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Posts
export function index(req, res) {
  return Post.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Post from the DB
export function show(req, res) {
  return Post.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Post in the DB
export function create(req, res) {
  var date = moment().format('MMMM Do YYYY, h:mm:ss a');
  req.body.date = date;
  return Post.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));

}

// Updates an existing Post in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Post.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Post from the DB
export function destroy(req, res) {
  return Post.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


//Kayode searches posts

export function search(req, res) {
  var searchString = req.body.searchString;
  console.log(searchString);
  var query = {
    $text: {$search: searchString}
  };
  var scoreStr = {
    score: {$meta: "textScore"}
  };
  var sortStr = {
    score: {$meta: "textScore"}
  };
  //Mongo 2.6
  //return Post.find(query, scoreStr).sort(sortStr).exec()
  return Post.textSearch(searchString, function (err, output) {
    if (err) {
      console.log(err);
      res.json(err);
    } else {
      res.status(200).json(output);
    }
  });
}
