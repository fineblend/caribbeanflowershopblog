'use strict';

angular.module('caribbeanflowershopblogApp.auth', ['caribbeanflowershopblogApp.constants',
    'caribbeanflowershopblogApp.util', 'ngCookies', 'ui.router'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
