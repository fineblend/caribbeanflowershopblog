'use strict';

angular.module('caribbeanflowershopblogApp')
  .directive('headerSmall', function () {
    return {
      templateUrl: 'components/headerSmall/headerSmall.html',
      restrict: 'EA',
      controller : function($scope){
      	var number;
      	number = String(Math.ceil(Math.random()*11));
      	$scope.myStyle = '{\'background-image\': \'url(assets/images/header/flower'+number+'.jpg)\', \'height\': \'350px\', \'background-repeat\': \'no-repeat\', \'background-size\': \'cover\' }';
      },
    };
  });
