'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'title': 'Blog',
    'state': 'main'
  }];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }

}

angular.module('caribbeanflowershopblogApp')
  .controller('NavbarController', NavbarController);
