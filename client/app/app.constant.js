(function(angular, undefined) {
'use strict';

angular.module('caribbeanflowershopblogApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);