'use strict';

(function(){

class PostComponent {
  constructor($http, $state, $stateParams, $window, $location) {
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$window = $window;
    this.$location = $location;
    this.searchTerm = '';
    this.post = {};
    this.config = { disqus_shortname:'blog-caribbeanflowershop-com',
                    disqus_identifier: 'post'+this.$stateParams.id,
                    disqus_url: this.$window.location.href
    };

  }

  goSearch(){
    let searchTerm = this.searchTerm;
    console.log('searchTerm', searchTerm);
    this.$state.go('main', {searchInput:searchTerm});
  }

  $onInit(){
    console.log('window_ref', this.$window.location.href);
    let id = this.$stateParams.id;
    this.$http({
      method: 'GET',
      url: '/api/post/'+id
    })
      .success(data=>{
        this.post = data;
        console.log(data);
      })
      .error(error=>{
        console.log('error', error);
      });
  }
}

angular.module('caribbeanflowershopblogApp')
  .component('post', {
    templateUrl: 'app/post/post.html',
    controller: PostComponent,
    controllerAs: 'post'
  });

})();
