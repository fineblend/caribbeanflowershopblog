'use strict';

angular.module('caribbeanflowershopblogApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('post', {
        url: '/post/:id',
        template: '<post></post>'
      });
  });
