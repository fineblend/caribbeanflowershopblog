'use strict';

describe('Component: EditorComponent', function () {

  // load the controller's module
  beforeEach(module('caribbeanflowershopblogApp'));

  var EditorComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    EditorComponent = $componentController('editor', {});
  }));

  it('should ...', function () {
    expect(1).to.equal(1);
  });
});
