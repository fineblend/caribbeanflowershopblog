'use strict';

angular.module('caribbeanflowershopblogApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('editor', {
        url: '/editor/:id',
        template: '<editor></editor>'
      });
  });
