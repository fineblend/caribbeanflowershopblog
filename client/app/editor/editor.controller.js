'use strict';

(function(){

class EditorComponent {
  constructor($http, $stateParams, $window, toastr) {
  	this.$http = $http;
  	this.$stateParams = $stateParams;
    this.$window = $window;
    this.toastr = toastr;
    this.message = 'Hello';
    this.post = {};
    this.options = {
      language: 'en',
      allowedContent: true,
      entities: false
    };
  }

  sendPost(){
    if(this.$stateParams.id) {
      var id = this.$stateParams.id;
      var data = this.post;
      this.$http({
        method: 'PUT',
        url: '/api/post/'+id,
        data : data
      })
      .success(data=>{
          this.post = data;
          console.log('data', data);
        this.toastr.success('Article edited');
      });
    }
    else {
      var imageValue = this.$window.document.getElementById('mainImage').files[0];
      console.log('imageValue', imageValue);

      var fd = new FormData();
      fd.append('mainImage', imageValue);
      console.log('fd', fd.mainImage);

      this.$http.post('/api/post/image', fd, {
        withCredentials: false,
        headers: {'Content-Type': undefined},
        transformRequest: angular.identity
      })
        .success(response => {
          this.post.mainImage = response;
          var data = this.post;
          this.$http({
              method: 'POST',
              url: '/api/post',
              data: data
            })
            .then(response => {
              console.log('response', response);
              this.toastr.success('Article posted');
            });
        });
    }
  }


  $onInit() {
    //get post if edit
    if(this.$stateParams.id){
      var id = this.$stateParams.id;
      this.$http({
        method: 'GET',
        url: '/api/post/'+id
        })
        .success(data=>{
          this.post = data;
          console.log('data', data);
        })

    }


    }
}

angular.module('caribbeanflowershopblogApp')
  .component('editor', {
    templateUrl: 'app/editor/editor.html',
    controller: EditorComponent,
    controllerAs: 'editor'
  });

})();
