'use strict';

angular.module('caribbeanflowershopblogApp', ['caribbeanflowershopblogApp.auth',
    'caribbeanflowershopblogApp.admin', 'caribbeanflowershopblogApp.constants', 'ngCookies',
    'ngResource', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'validation.match', 'ngAnimate', 'toastr', 'angularUtils.directives.dirDisqus', 'ckeditor'
  ])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
  });



