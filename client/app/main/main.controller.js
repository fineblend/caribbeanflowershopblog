'use strict';

(function() {

  class MainController {

    constructor($http, $stateParams, Auth, toastr) {
      this.$http = $http;
      this.$stateParams = $stateParams;
      this.isAdmin = Auth.isAdmin;
      this.toastr = toastr;
      this.awesomeThings = [];
      this.posts = [];
      this.item = {};
      this.searchInput = {};
    }

    remove(id){
      this.$http({
        method: 'DELETE',
        url: '/api/post/'+id
      })
        .success(data=> {
          this.posts = data;
          console.log('data', data);
        });
    }

    search(){
        var data = this.searchInput;
        console.log('data', data);
        this.$http({
          method: 'POST',
          url: '/api/post/search',
          data: data
          })
          .success(response => {
            console.log('response', response);
            this.posts = [];
            if(response.results){
                response.results.forEach(element=>this.posts.push(element.obj));
            }
            var numResults = this.posts.length;
            this.toastr.success(numResults+' search results');
            })
            .error(error=>console.log('error', error));
    }

    $onInit() {
      if (this.$stateParams.searchInput) {
        this.searchInput.searchString = String(this.$stateParams.searchInput);
        console.log('searchInput', this.searchInput);
        this.search();
      } else {

        this.$http({
          method: 'GET',
          url: '/api/post'
        })
          .success(data => {
            this.posts = data;
            console.log('data', data);
          })
          .error(error => {
            console.log('error', error);
          });
      }
    }
  }

  angular.module('caribbeanflowershopblogApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController,
      controllerAs : 'main'
    });
})();
