'use strict';

angular.module('caribbeanflowershopblogApp')
  .config(function($stateProvider) {
    $stateProvider.state('main', {
      url: '/:searchInput',
      template: '<main></main>'
    });
  });
